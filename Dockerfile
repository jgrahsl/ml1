FROM continuumio/anaconda3

RUN apt-get update
RUN apt-get install -y bzip2 build-essential

COPY environment.yml /
RUN conda env create -f /environment.yml

SHELL ["conda", "run", "-n", "azure", "/bin/bash", "-c"]

#RUN pip install azureml-sdk[explain,automl]

COPY requirements.txt /
RUN pip install -r /requirements.txt
RUN pip freeze

RUN ipython kernel install --user --name azure --display-name "Python (azure)"

#EXPOSE 8888