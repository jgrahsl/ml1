# Remote ML playground with anaconda, tensorflow and docker

## Quickstart for jupyter notebook

```
docker-compose build
make run
```

Check the logs to see what port juptyter is using, lines similiar to this one:

```
...
[I 23:18:15.778 NotebookApp] http://xxx:8889/?token=...
[I 23:18:15.778 NotebookApp]  or http://127.0.0.1:8889/?token=...
...
```

This indicates which address you should use to access the jupyter notebook.

In this case: `http://127.0.0.1:8889` (Note that the path and token parameter are stripped)

*Token/Password* for accessing the notebook is `notebook`.

## Contents

- a few ml example jupyter notebooks in `/src`
- `Dockerfile` building an `anaconda3` base image including
 - tensorflow
 - azuresdk
 - jupyter
- `docker-compose.yml` file defining services for
 - a service running a `jupyter` notebook.
 - a service dropping into a `bash` inside the container
- `pip` and `conda` dependency definitions:
 - `environment.yml` is used for conda based installs
 - `requirements.txt` is used for pip based installs
 
## Provides

- an image created by the `Dockerfile` including a decent set of packages (see Contents section)
- services defined in `docker-compose.yml` using the base image for
 - shell access to container
 - starting a jupyter notebook server using the password/token `notebook` for auth.

## Locations

- `src` folder is mounted as volume into the container as `/src`

## Usecases

### Build the image

```
docker-compose build
```

### Run a Jupyter notebook

```
make run
```

### Run a shell

```
make bash
```

